<?php

namespace MetaNull\RESTfulService\Network\Ip;

use \Exception;
use \Throwable;

class InvalidNetMaskException extends IpException
{
    public function __construct(string $message, int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
