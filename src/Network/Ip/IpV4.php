<?php

namespace MetaNull\RESTfulService\Network\Ip;

class IpV4
{

    /**
     * Converts an IPV4 address into an integer
     * @param  string $ipv4  IP to check in IPV4 format eg. 127.0.0.1
     * @return int
     */
    static function toLong(string $ipv4) : int
    {
        $long = ip2long($ipv4);
        if (false === $long) {
            throw new InvalidAddressException($ipv4);
        }
        return $long;
    }

    static function fromLong(int $long) : string
    {
        $string = long2ip($long);
        if (false === $string) {
            throw new InvalidAddressException((string)$long);
        }
        return $string;
    }

    static function isValid(string $ipv4) : bool
    {
        if (false === filter_var($ipv4, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            return false;
        }
        return true;
    }

    static function isValidNetMask(int $netmask) : bool
    {
        if (false === is_int($netmask)) {
            return false;
        }
        if ((int)$netmask < 0) {
            return false;
        }
        if ((int)$netmask > 32) {
            return false;
        }
        return true;
    }

    static function toIpRange(string $ipRange, int $defaultNetMask = 32) : string
    {
        if (false === strpos($ipRange, '/')) {
            $ipRange .= "/{$defaultNetMask}";
        }
        list( $ip, $netmask ) = explode('/', $ipRange, 2);
        if (false === self::isValid($ip)) {
            throw new InvalidAddressException($ipRange);
        }
        if (false === self::isValidNetMask($netmask)) {
            throw new InvalidNetMaskException($ipRange);
        }
        return $ipRange;
    }

    static function getIpFromRange(string $ipRange, int $defaultNetMask = 32) : string
    {
        list( $range, $netmask ) = explode('/', self::toIpRange($ipRange, $defaultNetMask), 2);
        return $range;
    }

    static function getNetmaskFromRange(string $ipRange, int $defaultNetMask = 32) : int
    {
        list( $range, $netmask ) = explode('/', self::toIpRange($ipRange, $defaultNetMask), 2);
        return (int)$netmask;
    }

    /**
     * Check if a given ip is in a network
     * @param string $ipv4  IP to check in IPV4 format eg. 127.0.0.1
     * @param string $range IP/CIDR netmask eg. 127.0.0.0/24, also 127.0.0.1 is accepted and /32 assumed
     * @param int $defaultNetMask The default network mask to use if omitted from the ipRange (default: /32)
     * @return boolean true if the ip is in this range / false if not.
     */
    static function inRange(string $ipv4, string $ipRange, int $defaultNetMask = 32) : bool
    {
        $rangeIp = self::getIpFromRange($ipRange, $defaultNetMask);
        $rangeNetmask = self::getNetmaskFromRange($ipRange, $defaultNetMask);
        
        $decimalRange = self::toLong($rangeIp);
        $decimalIP = self::toLong($ipv4);
        $decimalBitMask = pow(2, ( 32 - $rangeNetmask )) - 1;
        $decimalNetMask = ~ $decimalBitMask;
        return ( ( $decimalIP & $decimalNetMask ) == ( $decimalRange & $decimalNetMask ) );
    }
}
