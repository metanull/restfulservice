<?php
    namespace MetaNull\RESTfulService\Network\Http;
    
    use \JsonException;

    /**
     * Factory for Http requests. It facilitates the creation of well formed request, from PHP's environment, or from a HTTP REQUEST string
     *
     * @author Pascal Havelange (havelangep@hotmail.com)
     */
class RequestFactory
{
    private function __construct()
    {
        // Private constructor, prevents instanciation
    }

    /**
     * Creates a HTTP request by using the request currently being handled by PHP
     * @return Request
     */
    public static function FromCurrentRequest() : Request {
        $request = new Request();
        $request->firstLine = Http::BuildRequestFirstLine(implode('?',[$_SERVER['REQUEST_URI'],$_SERVER['QUERY_STRING']]),$_SERVER['REQUEST_METHOD'],$_SERVER['SERVER_PROTOCOL']);
        $request->headers = apache_request_headers();
        $request->body = null;
        $request->method = $_SERVER['REQUEST_METHOD'];
        $request->protocol = $_SERVER['SERVER_PROTOCOL'];
        $request->host = $_SERVER['HTTP_HOST'];
        $request->uri = $_SERVER['REQUEST_URI'];
        $request->queryString = $_SERVER['QUERY_STRING'];
        $request->parameters = $_GET;
        
        $request->parsedBody = null;
        $request->route = $request->parameters['route'] ?? null;
        unset($request->parameters['route']);
        $request->contentType = $request->headers[Http::HEADER_CONTENT_TYPE] ?? null;
            
        $ipArray = array_values(array_filter(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'] ?? ($_SERVER['REMOTE_ADDR'] ?? ''))));
        $clientIp = reset($ipArray);
        if (!filter_var($clientIp, FILTER_VALIDATE_IP)) {
            $clientIp = null;
        }
        $request->clientIp = $clientIp;

        if (in_array($request->method, [Http::POST, Http::PUT, Http::PATCH])) {
            if (Http::CONTENT_TYPE_MULTIPART_FORM_DATA != $request->contentType) {
                $payload = file_get_contents("php://input");
                $request->body = ($payload === false ? null : $payload);
                $request->parsedBody = self::ParseBody($request->contentType,$request->body);
            }
        }
        return $request;
    }
       
    /**
     * Creates a HTTP request object by parsing a HTTP Request string
     * @param string $httpRequest The complete text of a HTTP request. By example: "GET /example HTTP/1.1\r\nHost: example.com\r\n\r\n"
     * @return Request
     */
    public static function FromString($httpRequest) : Request {
        // Split the request into an array of lines
        $lines = explode("\r\n", $httpRequest);

        // Extract the first line, which should contain the request method, URI, and version
        $first_line = array_shift($lines);
        [$method, $uri, $version] = explode(' ', $first_line, 3) + ['', '', ''];

        // Extract the headers into an associative array
        $headers = [];
        foreach ($lines as $line) {
            if(0 === strlen($lines[0])) {
                break;
            }
            if (strpos($line, ':') === false) {
                break;
            }
            [$key, $value] = explode(': ', $line, 2);
            $headers[$key] = $value;
            array_shift($lines);
        }

        // Extract the body, if there is one
        $body = implode("\r\n", $lines);

        // Extract the Query String from the URI
        [$uri, $query_string] = explode('?', $uri, 2) + ['', ''];

        // Populate the request object
        $request = new Request();
        $request->firstLine = $first_line;
        $request->headers = $headers;
        $request->body = $body;

        $request->method = $method;
        $request->protocol = $version;
        $request->host = $headers[Http::HEADER_HOST] ?? null;
        $request->uri = $uri;
        $request->queryString = $query_string;
        parse_str($query_string, $request->parameters);
        $request->parsedBody = null;
        $request->route = $request->parameters['route'] ?? null;
        unset($request->parameters['route']);
        $request->contentType = $headers[Http::HEADER_CONTENT_TYPE] ?? null;

        $ipArray = array_values(array_filter(explode(',', $headers['HTTP_X_FORWARDED_FOR'] ?? '')));
        $clientIp = reset($ipArray);
        if (!filter_var($clientIp, FILTER_VALIDATE_IP)) {
            $clientIp = null;
        }
        $request->clientIp = $clientIp;

        if (!empty($request->body)) {
            $request->parsedBody = self::ParseBody($request->contentType,$request->body);
        }

        return $request;
    }

    /**
     * Constructs a Request object for the request currently being processed by PHP
     */
    public static function ParseBody(string $contentType, string $body) : ?array
    {
        $parsedBody = null;
        switch ($contentType) {
            case Http::CONTENT_TYPE_X_WWW_FORM_URLENCODED:
                $parsedBody = Http::DecodeWwwUrlEncoded($body);
                break;
            case Http::CONTENT_TYPE_APPLICATION_JSON:
                try {
                    $parsedBody = Http::DecodeJson($body);
                } catch (JsonException $e) {
                    $parsedBody = null;
                }
                break;
            case Http::CONTENT_TYPE_MULTIPART_FORM_DATA:
                $parsedBody = null;
                break;
            default:
                $parsedBody = null;
        }
        return $parsedBody;
    }
}
