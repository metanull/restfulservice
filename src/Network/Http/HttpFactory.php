<?php
    namespace MetaNull\RESTfulService\Network\Http;
    
    /**
     * Factory for the creation of Http "Responses" and "Requests"
     *
     * @see https://restfulapi.net/http-status-codes/
     * @author Pascal Havelange (havelangep@hotmail.com)
     */
class HttpFactory
{
    private function __construct()
    {
        // Private constructor, prevents instanciation
    }
    
    /**
     * Create a new HTTP Response object
     * @param int $code HTTP status code
     * @param string $phrase HTTP status phrase
     * @param string $protocol HTTP protocol version
     * @param string $body Body of the HTTP response (set to NULL if the response shall not include a body)
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header). If more than one array is passed to the function, they are merged into one array, in the received order
     * @return Http
     */
    public static function Response(int $code = Http::OK, string $phrase = Http::OK, string $protocol = Http::HTTP1_1, string $body = null, array ...$headers)
    {
        return new Http(Http::BuildResponseFirstLine($code, $phrase, $protocol), $body, ...$headers);
    }
       
    /**
     * Create a new HTTP Request object
     * @param string $Uri URI of the resource (e.g.: /)
     * @param string $verb HTTP Verb (e.g.: GET)
     * @param string $protocol HTTP protocol version
     * @param string $body Body of the HTTP response (set to NULL if the response shall not include a body)
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header). If more than one array is passed to the function, they are merged into one array, in the received order
     * @return Http
     */
    public static function Request(string $Uri = '/', string $verb = Http::GET, string $protocol = Http::HTTP1_1, string $body = null, array ...$headers)
    {
        return new Http(Http::BuildRequestFirstLine($Uri, $verb, $protocol), $body, ...$headers);
    }
}
