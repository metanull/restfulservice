<?php
    namespace MetaNull\RESTfulService\Network\Http;
    
    /**
     * Encapsulates a HTTP Response
     * You may use ResponseFactory to a variety of HTTP responses
     *
     * @author Pascal Havelange (havelangep@hotmail.com)
     * @see ResponseFactory
     */
class Response extends Http
{
    /** @var int The HTTP status code of the response */
    public int $code;
        
    /**
     * @param int $code HTTP status code (e.g.: 200)
     * @param string $body Body of the HTTP response (set NULL if the response shall not include a body)
     * @param array ...$headers One (or more) associative arrays of headers (key = header name, value = header value)
     */
    public function __construct(int $code = null, string $body = null, array ...$headers)
    {
        parent::__construct(Http::BuildResponseFirstLine($code));
        $this->code = $code;
        $this->body = $body;
        $this->headers = array_merge(...$headers);
    }
    
    /**
     * Sends the HTTP response (this is equivalent of calling header() on every defined ehader, then outputting the response's body to the standard output)
     * @return bool Returns true in case of success. Otherwise, false is returned, and a warning is triggered.
     */
    public function Send() : bool
    {
        $output_started_file = null;
        $output_started_line = null;
        if (headers_sent($output_started_file, $output_started_line)) {
            trigger_error(E_USER_WARNING, sprintf('Application has already outputted some content, it is impossible to add new HTTP headers. Output started in %s on line %d', $output_started_file, $output_started_line));
            return false;
        }
        foreach ($this->headers as $header => $value) {
            $headerLine = sprintf('%s: %s', $header, $value);
            header($headerLine);
        }
        http_response_code($this->code);
        if ($this->body === null || strlen($this->body)) {
            echo $this->body;
        }
        return true;
    }
}
