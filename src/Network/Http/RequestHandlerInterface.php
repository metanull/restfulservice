<?php

namespace MetaNull\RESTfulService\Network\Http;

use MetaNull\RESTfulService\Network\Http\Request;
use MetaNull\RESTfulService\Network\Http\Response;
use MetaNull\RESTfulService\Network\Http\HttpException;

/**
 * A RequestHandler provides a method to handle a HTTP Request, this processing produces a HTTP Response
 * @author Pascal Havelange (havelangep@hotmail.com)
 */
interface RequestHandlerInterface
{
    
    /**
     * A RequestHandler provides a method to handle a HTTP Request, this processing produces a HTTP Response
     * @param Request $request The received HTTP request
     * @param Response &$response The current response, it can be modified
     * @param string $routeParameters,... May receive parameters that were extracted from the Request's Route)
     * @throws HttpException
     */
    public function Handle(Request $request, Response & $response, string ...$routeParameters);
}
