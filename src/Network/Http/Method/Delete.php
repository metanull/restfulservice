<?php

namespace MetaNull\RESTfulService\Network\Http\Method;

use MetaNull\RESTfulService\Network\Http\RequestHandlerInterface;

/** A marker showing that this Handler is appropriate to handle HTTP DELETE requests */
interface Delete extends RequestHandlerInterface
{

}
