<?php
    namespace MetaNull\RESTfulService\Network\Http;
    
    /**
     * Factory for Http responses. It facilitates the creation of well formed response, and the handling of HTTP status codes
     *
     * @see https://restfulapi.net/http-status-codes/
     * @author Pascal Havelange (havelangep@hotmail.com)
     */
class ResponseFactory
{
    private function __construct()
    {
        // Private constructor, prevents instanciation
    }
    
    /**
     * HTTP 200 - Create a response with and empty body, that indicates that the request has succeeded.
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function OK(array ...$headers)
    {
        return new Response(Http::OK, null, ...$headers);
    }

    /**
     * HTTP 200 - Create a response with an application/x-www-form-urlencoded body, that indicates that the request has succeeded.
     * @param array $response Array of data to return after URL encoding
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function WwwUrlEncoded(array $response, array ...$headers)
    {
        $contentType = [Http::HEADER_CONTENT_TYPE => Http::CONTENT_TYPE_X_WWW_FORM_URLENCODED];
        return new Response(Http::OK, Http::EncodeWwwUrlEncoded($response), $contentType, ...$headers);
    }
        
    /**
     * HTTP 200 - Create a JSON response (with an application/json body), that indicates that the request has succeeded.
     * @param array $response Array of data to return after JSON encoding
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function Json(array $response, array ...$headers)
    {
        $contentType = [Http::HEADER_CONTENT_TYPE => Http::CONTENT_TYPE_APPLICATION_JSON];
        return new Response(Http::OK, Http::EncodeJson($response), $contentType, ...$headers);
    }
        
    /**
     * HTTP 200 - Create a plain text response, that indicates that the request has succeeded.
     * @param string $response Data to return in the response
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function Text(string $response, array ...$headers)
    {
        $contentType = [Http::HEADER_CONTENT_TYPE => Http::CONTENT_TYPE_TEXT_PLAIN];
        return new Response(Http::OK, $response, $contentType, ...$headers);
    }

    /**
     * HTTP 200 - Create a HTML response, that indicates that the request has succeeded.
     * @param string $response Data to return in the response, it should be compliant with HTML standards
     * @param string $encoding Character set of the response, if null or ommitted, "utf-8" is assumed. The character set is reported in the Content-Type header of the response
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function Html(string $response, ?string $encoding = null, array ...$headers)
    {
        $headerValue = Http::CONTENT_TYPE_TEXT_HTML_UTF8;
        if (null !== $encoding) {
            $headerValue = sprintf('%s; charset=%s', Http::CONTENT_TYPE_TEXT_HTML, $encoding);
        }
        $contentType = [Http::HEADER_CONTENT_TYPE => $headerValue];
        return new Response(Http::OK, $response, $contentType, ...$headers);
    }

    /**
     * HTTP 201 - Indicates that the request has succeeded and a new resource has been created as a result.
     * @param string $Uri URI of the created resource
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function Created(string $Uri, array ...$headers)
    {
        $redirect = [Http::HEADER_LOCATION => $Uri];
        return new Response(Http::CREATED, null, $redirect, ...$headers);
    }

    /**
     * HTTP 202 - Indicates that the request has been received but not completed yet. It is typically used in log running requests and batch processing.
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function Accepted(array ...$headers)
    {
        return new Response(Http::ACCEPTED, null, ...$headers);
    }

    /**
     * HTTP 204 - The server has fulfilled the request but does not need to return a response body. The server may return the updated meta information.
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function NoContent(array ...$headers)
    {
        return new Response(Http::NO_CONTENT, null, ...$headers);
    }
        
    /**
     * HTTP 301 - The URL of the requested resource has been changed permanently. The new URL is given by the Location header field in the response. This response is cacheable unless indicated otherwise.
     * @param string $Uri URI of the resource
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function MovedPermanently(string $Uri, array ...$headers)
    {
        $redirect = [Http::HEADER_LOCATION => $Uri];
        return new Response(Http::MOVED_PERMANENTLY, null, $redirect, ...$headers);
    }

    /**
     * HTTP 302 - The URL of the requested resource has been changed temporarily. The new URL is given by the Location field in the response. This response is only cacheable if indicated by a Cache-Control or Expires header field.
     * @param string $Uri URI of the resource
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function Found(string $Uri, array ...$headers)
    {
        $redirect = [Http::HEADER_LOCATION => $Uri];
        return new Response(Http::FOUND, null, $redirect, ...$headers);
    }

    /**
     * HTTP 303 - The response can be found under a different URI and SHOULD be retrieved using a GET method on that resource.
     * @param string $Uri URI of the resource
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function SeeOther(string $Uri, array ...$headers)
    {
        $redirect = [Http::HEADER_LOCATION => $Uri];
        return new Response(Http::SEE_OTHER, null, $redirect, ...$headers);
    }

    /**
     * HTTP 304 - Indicates the client that the response has not been modified, so the client can continue to use the same cached version of the response.
     * @param string $Uri URI of the resource
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function NotModified(string $Uri, array ...$headers)
    {
        $redirect = [Http::HEADER_LOCATION => $Uri];
        return new Response(Http::NOT_MODIFIED, null, $redirect, ...$headers);
    }

    /**
     * HTTP 307 - Indicates the client to get the requested resource at another URI with same method that was used in the prior request. It is similar to 302 Found with one exception that the same HTTP method will be used that was used in the prior request.
     * @param string $Uri URI of the resource
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function TemporaryRedirect(string $Uri, array ...$headers)
    {
        $redirect = [Http::HEADER_LOCATION => $Uri];
        return new Response(Http::TEMPORARY_REDIRECT, null, $redirect, ...$headers);
    }

    /**
     * HTTP 308 - Indicates that the resource is now permanently located at another URI, specified by the Location header. It is similar to 301 Moved Permanently with one exception that the same HTTP method will be used that was used in the prior request.
     * @param string $Uri URI of the resource
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function PermanentRedirect(string $Uri, array ...$headers)
    {
        $redirect = [Http::HEADER_LOCATION => $Uri];
        return new Response(Http::PERMANENT_REDIRECT, null, $redirect, ...$headers);
    }

    /**
     * HTTP 400 - The request could not be understood by the server due to incorrect syntax. The client SHOULD NOT repeat the request without modifications.
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function BadRequest(array ...$headers)
    {
        return new Response(Http::BAD_REQUEST, null, ...$headers);
    }

    /**
     * HTTP 404 - The server can not find the requested resource.
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function NotFound(array ...$headers)
    {
        return new Response(Http::NOT_FOUND, null, ...$headers);
    }

    /**
     * HTTP 401 - Unauthorized request. The client does not have access rights to the content. Unlike 401, the client’s identity is known to the server.
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function Forbidden(array ...$headers)
    {
        return new Response(Http::FORBIDDEN, null, ...$headers);
    }

    /**
     * HTTP 403 - Indicates that the request requires user authentication information. The client MAY repeat the request with a suitable Authorization header field
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function Unauthorized(array ...$headers)
    {
        return new Response(Http::UNAUTHORIZED, null, ...$headers);
    }

    /**
     * HTTP 405 - The request HTTP method is known by the server but has been disabled and cannot be used for that resource.
     * @param array $allowedMethods A list of methods (aka. "verbs") that are supported for that resource (e.g.: ['GET','POST'])
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function MethodNotAllowed(array $allowedMethods, array ...$headers)
    {
        $allow = [Http::HEADER_ALLOW => implode(',', $allowedMethods)];
        return new Response(Http::METHOD_NOT_ALLOWED, null, $allow, ...$headers);
    }

    /**
     * HTTP 406 - The server doesn’t find any content that conforms to the criteria given by the user agent in the Accept header sent in the request.
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function NotAcceptable(array ...$headers)
    {
        return new Response(Http::NOT_ACCEPTABLE, null, ...$headers);
    }

    /**
     * HTTP 412 - The client has indicated preconditions in its headers which the server does not meet.
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function PreconditionFailed(array ...$headers)
    {
        return new Response(Http::PRECONDITION_FAILED, null, ...$headers);
    }

    /**
     * HTTP 415 - The media-type in Content-type of the request is not supported by the server.
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function UnsupportedMediaType(array ...$headers)
    {
        return new Response(Http::UNSUPPORTED_MEDIA_TYPE, null, ...$headers);
    }

    /**
     * HTTP 429 - The user has sent too many requests in a given amount of time (“rate limiting”).
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function TooManyRequests(array ...$headers)
    {
        return new Response(Http::TOO_MANY_REQUESTS, null, ...$headers);
    }
    
    /**
     * HTTP 500 - The server encountered an unexpected condition that prevented it from fulfilling the request.
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function InternalServerError(array ...$headers)
    {
        return new Response(Http::INTERNAL_SERVER_ERROR, null, ...$headers);
    }

    /**
     * HTTP 501 - The HTTP method is not supported by the server and cannot be handled.
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function NotImplemented(array ...$headers)
    {
        return new Response(Http::NOT_IMPLEMENTED, null, ...$headers);
    }

    /**
     * HTTP 502 - The server got an invalid response while working as a gateway to get the response needed to handle the request.
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function BadGateway(array ...$headers)
    {
        return new Response(Http::BAD_GATEWAY, null, ...$headers);
    }

    /**
     * HTTP 503 - The server is not ready to handle the request.
     * @param array $headers HTTP headers for the response as an associative array where keys are the Header's name and value the Header's value (set to NULL if the response shall not include any extra header)
     */
    public static function ServiceUnavailable(array ...$headers)
    {
        return new Response(Http::SERVICE_UNAVAILABLE, null, ...$headers);
    }
}
