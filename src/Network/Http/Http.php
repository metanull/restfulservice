<?php
    namespace MetaNull\RESTfulService\Network\Http;

    use \LogicException;
    
    /**
     * Encapsulates a HTTP Message (Request or Response)
     *
     * @author Pascal Havelange (havelangep@hotmail.com)
     */
class Http
{
    /** @var string HTTP first line's version: "HTTP 1.0" */
    const HTTP1_0 = 'HTTP/1.0';
    /** @var string HTTP first line's version: "HTTP 1.0" */
    const HTTP1 = 'HTTP/1.0';
    /** @var string HTTP first line's version: "HTTP 1.1" */
    const HTTP1_1 = 'HTTP/1.1';
    /** @var string HTTP first line's version: "HTTP 2" */
    const HTTP2_0 = 'HTTP/2';
    /** @var string HTTP first line's version: "HTTP 2" */
    const HTTP2 = 'HTTP/2';
        
    /** @var string HTTP first line's verb: "GET" */
    const GET = 'GET';
    /** @var string HTTP first line's verb: "POST" */
    const POST = 'POST';
    /** @var string HTTP first line's verb: "PUT" */
    const PUT = 'PUT';
    /** @var string HTTP first line's verb: "PATCH" */
    const PATCH = 'PATCH';
    /** @var string HTTP first line's verb: "DELETE" */
    const DELETE = 'DELETE';
    /** @var string HTTP first line's verb: "OPTIONS" */
    const OPTIONS = 'OPTIONS';
    /** @var string HTTP first line's verb: "TRACE" */
    const TRACE = 'TRACE';
    /** @var string HTTP first line's verb: "HEAD" */
    const HEAD = 'HEAD';

    /** @var string HTTP "Host" header */
    const HEADER_HOST = 'Host';
    /** @var string HTTP "Location" header */
    const HEADER_LOCATION = 'Location';
    /** @var string HTTP "Content-Type" header */
    const HEADER_CONTENT_TYPE = 'Content-Type';
    /** @var string Content-Type: "application/x-www-form-urlencoded" */
    const CONTENT_TYPE_X_WWW_FORM_URLENCODED = 'application/x-www-form-urlencoded';
    /** @var string Content-Type: "application/json" */
    const CONTENT_TYPE_APPLICATION_JSON = 'application/json';
    /** @var string Content-Type: "text/plain" */
    const CONTENT_TYPE_TEXT_PLAIN = 'text/plain';
    /** @var string Content-Type: "text/html" */
    const CONTENT_TYPE_TEXT_HTML = 'text/html';
    /** @var string Content-Type: "text/html; charset=utf-8" */
    const CONTENT_TYPE_TEXT_HTML_UTF8 = 'text/html; charset=utf-8';
    /** @var string Content-Type: "text/html; charset=utf-8" */
    const CONTENT_TYPE_MULTIPART_FORM_DATA = 'multipart/form-data';
    /** @var string HTTP "Access-Control-Allow-Origin" header */
    const HEADER_ACCESS_CONTROL_ALLOW_ORIGIN = 'Access-Control-Allow-Origin';
    /** @var string Access-Control-Allow-Origin: "*" */
    const ALLOW_ORIGIN_ANY = '*';
    /** @var string HTTP "Vary" header */
    const HEADER_VARY = 'Vary';
    /** @var string Vary: Origin */
    const VARY_ORIGIN = 'Origin';
    /** @var string HTTP "Access-Control-Allow-Methods" header */
    const HEADER_ACCESS_CONTROL_ALLOW_METHODS = 'Access-Control-Allow-Methods';
    /** @var string HTTP "Access-Control-Allow-Headers" header */
    const HEADER_ACCESS_CONTROL_ALLOW_HEADERS = 'Access-Control-Allow-Headers';
    /** @var string HTTP "Access-Control-Request-Method" header */
    const HEADER_ACCESS_CONTROL_REQUEST_METHOD = 'Access-Control-Request-Method';
    /** @var string HTTP "Access-Control-Request-Headers" header */
    const HEADER_ACCESS_CONTROL_REQUEST_HEADERS = 'Access-Control-Request-Headers';
    /** @var string HTTP "Allow" header */
    const HEADER_ALLOW = 'Allow';

    /** @var int HTTP status code 200 "OK" */
    const OK = 200;
    /** @var int HTTP status code 201 "Created" */
    const CREATED = 201;
    /** @var int HTTP status code 202 "Accepted" */
    const ACCEPTED = 202;
    /** @var int HTTP status code 204 "No Content" */
    const NO_CONTENT = 204;
                
    /** @var int HTTP status code 301 "Moved Permanently" */
    const MOVED_PERMANENTLY = 301;
    /** @var int HTTP status code 302 "Found" */
    const FOUND = 302;
    /** @var int HTTP status code 303 "See Other" */
    const SEE_OTHER = 303;
    /** @var int HTTP status code 304 "Not Modified" */
    const NOT_MODIFIED = 304;
    /** @var int HTTP status code 307 "Temporary Redirect" */
    const TEMPORARY_REDIRECT = 307;
    /** @var int HTTP status code 308 "Permanent Redirect" */
    const PERMANENT_REDIRECT = 308;
                    
    /** @var int HTTP status code 400 "Bad Request" */
    const BAD_REQUEST = 400;
    /** @var int HTTP status code 401 "Unauthorized" */
    const UNAUTHORIZED = 401;
    /** @var int HTTP status code 403 "Forbidden" */
    const FORBIDDEN = 403;
    /** @var int HTTP status code 404 "Not Found" */
    const NOT_FOUND = 404;
    /** @var int HTTP status code 405 "Method Not Allowed" */
    const METHOD_NOT_ALLOWED = 405;
    /** @var int HTTP status code 406 "Not Acceptable" */
    const NOT_ACCEPTABLE = 406;
    /** @var int HTTP status code 412 "Precondition Failed" */
    const PRECONDITION_FAILED = 412;
    /** @var int HTTP status code 415 "Unsupported Media Type" */
    const UNSUPPORTED_MEDIA_TYPE = 415;
    /** @var int HTTP status code 429 "Too Many Requests" */
    const TOO_MANY_REQUESTS = 429;
                
    /** @var int HTTP status code 500 "Internal Server Error" */
    const INTERNAL_SERVER_ERROR = 500;
    /** @var int HTTP status code 501 "Not Implemented" */
    const NOT_IMPLEMENTED = 501;
    /** @var int HTTP status code 502 "Bad Gateway" */
    const BAD_GATEWAY = 502;
    /** @var int HTTP status code 503 "Service Unavailable" */
    const SERVICE_UNAVAILABLE = 503;
    /** @var int HTTP status code 504 "Gateway Timeout" */
    const GATEWAY_TIMEOUT = 504;
    /** @var int HTTP status code 505 "HTTP Version Not Supported" */
    const HTTP_VERSION_NOT_SUPPORTED = 505;
    
        /** @var string HTTP status phrase 200 "OK" */
    const OK_PHRASE = 'OK';
    /** @var string HTTP status phrase 201 "Created" */
    const CREATED_PHRASE = 'Created';
    /** @var string HTTP status phrase 202 "Accepted" */
    const ACCEPTED_PHRASE = 'Accepted';
    /** @var string HTTP status phrase 204 "No Content" */
    const NO_CONTENT_PHRASE = 'No Content';
                
    /** @var string HTTP status phrase 301 "Moved Permanently" */
    const MOVED_PERMANENTLY_PHRASE = 'Moved Permanently';
    /** @var string HTTP status phrase 302 "Found" */
    const FOUND_PHRASE = 'Found';
    /** @var string HTTP status phrase 303 "See Other" */
    const SEE_OTHER_PHRASE = 'See Other';
    /** @var string HTTP status phrase 304 "Not Modified" */
    const NOT_MODIFIED_PHRASE = 'Not Modified';
    /** @var string HTTP status phrase 307 "Temporary Redirect" */
    const TEMPORARY_REDIRECT_PHRASE = 'Temporary Redirect';
    /** @var string HTTP status phrase 308 "Permanent Redirect" */
    const PERMANENT_REDIRECT_PHRASE = 'Permanent Redirect';
                    
    /** @var string HTTP status phrase 400 "Bad Request" */
    const BAD_REQUEST_PHRASE = 'Bad Request';
    /** @var string HTTP status phrase 401 "Unauthorized" */
    const UNAUTHORIZED_PHRASE = 'Unauthorized';
    /** @var string HTTP status phrase 403 "Forbidden" */
    const FORBIDDEN_PHRASE = 'Forbidden';
    /** @var string HTTP status phrase 404 "Not Found" */
    const NOT_FOUND_PHRASE = 'Not Found';
    /** @var string HTTP status phrase 405 "Method Not Allowed" */
    const METHOD_NOT_ALLOWED_PHRASE = 'Method Not Allowed';
    /** @var string HTTP status phrase 406 "Not Acceptable" */
    const NOT_ACCEPTABLE_PHRASE = 'Not Acceptable';
    /** @var string HTTP status phrase 412 "Precondition Failed" */
    const PRECONDITION_FAILED_PHRASE = 'Precondition Failed';
    /** @var string HTTP status phrase 415 "Unsupported Media Type" */
    const UNSUPPORTED_MEDIA_TYPE_PHRASE = 'Unsupported Media Type';
    /** @var string HTTP status phrase 429 "Too Many Requests" */
    const TOO_MANY_REQUESTS_PHRASE = 'Too Many Requests';
                
    /** @var string HTTP status phrase 500 "Internal Server Error" */
    const INTERNAL_SERVER_ERROR_PHRASE = 'Internal Server Error';
    /** @var string HTTP status phrase 501 "Not Implemented" */
    const NOT_IMPLEMENTED_PHRASE = 'Not Implemented';
    /** @var string HTTP status phrase 502 "Bad Gateway" */
    const BAD_GATEWAY_PHRASE = 'Bad Gateway';
    /** @var string HTTP status phrase 503 "Service Unavailable" */
    const SERVICE_UNAVAILABLE_PHRASE = 'Service Unavailable';
    /** @var string HTTP status phrase 504 "Gateway Timeout" */
    const GATEWAY_TIMEOUT_PHRASE = 'Gateway Timeout';
    /** @var string HTTP status phrase 505 "HTTP Version Not Supported" */
    const HTTP_VERSION_NOT_SUPPORTED_PHRASE = 'HTTP Version Not Supported';

        
    /** @var string First line of the HTTP message */
    public ?string $firstLine = null;

    /** @var array Headers of the HTTP message in an associative array (key = header name, value = header value)*/
    public ?array $headers = null;

    /** @var string Body of the HTTP message */
    public ?string $body = null;

    /** @var array Associative array of "pairs" of HTTP status code and HTTP status phrase (it is initialized only once) */
    private static ?array $status = null;
        
    /**
     * Construct a new Http message
     * @param string $firstLine First line of the HTTP message
     * @param string $body Body of the HTTP message
     * @param array ...$headers Array(s) of headers of the HTTP message, as an associative array (key = header name, value = header value). If more than one array is passed to the function, they are merged into one array, in the received order.
     */
    public function __construct(string $firstLine = null, string $body = null, array ...$headers)
    {
        $this->firstLine = $firstLine;
        $this->body = $body;
        $this->headers = array_merge(...$headers);
    }
        
    /**
     * Build the first line of a HTTP "Response" message
     * @param int $code HTTP status code (must be >= 100 and <= 599)
     * @param string|null $phrase Phrase for the response (e.g.: OK)
     * @param string $protocol HTTP Version number (e.g.: "HTTP/1.1")
     * @return string
     */
    public static function BuildResponseFirstLine(int $code, string $phrase = null, string $protocol = self::HTTP1_1) : string
    {
        if (!($code >= 100 && $code <= 600)) {
            throw new LogicException('Http response requires a valid "status code"');
        }
        if (null === ($phrase ?? self::StatusPhrase($code))) {
            throw new LogicException('Http response requires a valid "phrase", NULL given and the "phrase" can\'t be inferred from the provided "status code"');
        }
        return sprintf('%s %d %s', $protocol, $code, $phrase ?? self::StatusPhrase($code));
    }
        
    /**
     * Build the first line of a HTTP "Request" message
     * @param string $Uri URI of the resource (e.g.: /)
     * @param string $verb HTTP Verb (e.g.: GET)
     * @param string $protocol HTTP Version number (e.g.: "HTTP/1.1")
     * @return string
     */
    public static function BuildRequestFirstLine(string $Uri, string $verb = self::GET, string $protocol = self::HTTP1_1)
    {
        return sprintf('%s %s %s', $verb, $Uri, $protocol);
    }
    
    /**
     * Print or return the HTTP message
     * @param bool $return If True, the message is returned, otherwise it is printed to standard output
     * @return string
     */
    public function Dump($return = false)
    {
        $dump = '';
        $dump .= $this->firstLine;
        $dump .= "\r\n";
        foreach ($this->headers as $header => $value) {
            $dump .= sprintf("%s: %s\r\n", $header, $value);
        }
        if ($this->body !== null) {
            $dump .= sprintf("\r\n%s", $this->body);
        }
        if ($return) {
            return $dump;
        } else {
            echo $dump;
        }
    }

    /**
     * Get the value of a HTTP header
     * @param string $name Name of the header
     * @param string $default The default value to return if the header is not set (default ::= null)
     * @return string|null Returns the header's value, or null if the header does not exist
     */
    public function GetHeader(string $name, ?string $default = null) : ?string
    {
        return $this->headers[$name] ?? $default;
    }

    /**
     * Get all the HTTP headers
     * @return array Returns the array of headers
     */
    public function GetHeaders() : ?array
    {
        return $this->headers;
    }

    /**
     * Set the value of a HTTP header
     * @param string $name Name of the header
     * @param string $value Value of the header
     */
    public function SetHeader(string $name, string $value)
    {
        $this->headers[$name] = $value;
    }

    /**
     * Remove a header from the list
     * @param string $name Name of the header
     */
    public function UnsetHeader(string $name)
    {
        if (array_key_exists($name, $this->headers)) {
            unset($this->headers[$name]);
        }
    }

    /**
     * Add several headers to the list, replacing exisiting if any.
     * @param array $headers An associative array of headers (key = header name, value = header value)
     */
    public function AppendHeaders(array ...$headers)
    {
        $this->headers = array_merge($this->headers, ...$headers);
    }

    /**
     * Remove all headers
     */
    public function ResetHeaders()
    {
        $this->headers = [];
    }
        
    /**
     * Generate URL-encoded query string from an array
     * @param array $payload It may be a simple one-dimensional structure, or an array of arrays (which in turn may contain other arrays).
     * @return string
     * @see http_build_query
     */
    public static function EncodeWwwUrlEncoded(array $payload) : string
    {
        return http_build_query($payload);
    }
    /**
     * Parses string as if it were the query string passed via a URL
     * @param string $payload The input string
     * @return array
     * @see parse_str
     */
    public static function DecodeWwwUrlEncoded(string $payload) : array
    {
        $decoded = null;
        parse_str($payload, $decoded);
        return $decoded;
    }
        
    /**
     * Decodes a JSON string
     * @param string $payload The json string being decoded (This function only works with UTF-8 encoded strings).
     * @return array Returns the value encoded in json as an associative array (JSON objects are returned as associative arrays)
     * @see json_decode
     * @throws \JsonException
     */
    public static function DecodeJson(string $payload) : array
    {
        $decoded = json_decode($payload, true, JSON_OBJECT_AS_ARRAY | JSON_THROW_ON_ERROR);
        return $decoded;
    }
    /**
     * Returns the JSON representation of an array
     * @param array $payload The value being encoded. Can be any type except a resource (all string data must be UTF-8 encoded).
     * @return string Returns a JSON encoded string
     * @see json_encode
     * @throws \JsonException
     */
    public static function EncodeJson(array $payload) : string
    {
        return json_encode($payload, JSON_THROW_ON_ERROR);
    }
        
    /**
     * Return the HTTP status phrase for a given HTTP status code
     * @param int $code HTTP status code (e.g.: 200)
     * @return string|null Returns the HTTP status phrase, or NULL if the status code is not known.
     */
    public static function StatusPhrase(int $code) : string
    {
        $statuses = self::GetStatuses();
        return $statuses[$code] ?? null;
    }
        
    /**
     * Return the HTTP status code for a given HTTP status phrase
     * @param string $phrase HTTP status message (e.g.: OK)
     * @return int|null Reeturns the HTTP status code, OR null if the status phrase is not known.
     */
    public static function StatusCode(string $phrase) : int
    {
        $statuses = self::GetStatuses();
        $key = array_search($phrase, $statuses, true);
        return $key === false ? null : $key;
    }
        
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Status#information_responses
    # http://en.wikipedia.org/wiki/List_of_HTTP_status_codes

    /**
     * Return an associative array of "pairs" of HTTP status code and HTTP status phrase
     * @return array
     */
    public static function GetStatuses() : array
    {
        if (self::$status === null) {
            self::$status = [
                100 => 'Continue',
                101 => 'Switching Protocols',
                102 => 'Processing', // WebDAV
                103 => 'Early Hints',
                    
                200 => 'OK',
                201 => 'Created',
                202 => 'Accepted',
                203 => 'Non-Authoritative Information',
                204 => 'No Content',
                205 => 'Reset Content',
                206 => 'Partial Content',
                207 => 'Multi-Status', // WebDAV
                208 => 'Already Reported', // WebDAV
                226 => 'IM Used', // HTTP Delta encoding
                    
                300 => 'Multiple Choices',
                301 => 'Moved Permanently',
                302 => 'Found',
                303 => 'See Other',
                304 => 'Not Modified',
                305 => 'Use Proxy',
                306 => 'unused',
                307 => 'Temporary Redirect',
                308 => 'Permanent Redirect',
                    
                400 => 'Bad Request',
                401 => 'Unauthorized',
                402 => 'Payment Required',
                403 => 'Forbidden',
                404 => 'Not Found',
                405 => 'Method Not Allowed',
                406 => 'Not Acceptable',
                407 => 'Proxy Authentication Required',
                408 => 'Request Timeout',
                409 => 'Conflict',
                410 => 'Gone',
                411 => 'Length Required',
                412 => 'Precondition Failed',
                413 => 'Request Entity Too Large',
                414 => 'Request-URI Too Long',
                415 => 'Unsupported Media Type',
                416 => 'Requested Range Not Satisfiable',
                417 => 'Expectation Failed',
                418 => 'I\'m a teapot', // RFC 2324
                419 => 'Authentication Timeout', // not in RFC 2616
                420 => 'Method Failure', // Spring Framework
                420 => 'Enhance Your Calm', // Twitter
                422 => 'Unprocessable Entity', // WebDAV; RFC 4918
                423 => 'Locked', // WebDAV; RFC 4918
                424 => 'Failed Dependency', // WebDAV; RFC 4918
                424 => 'Method Failure', // WebDAV)
                425 => 'Unordered Collection', // Internet draft
                426 => 'Upgrade Required', // RFC 2817
                428 => 'Precondition Required', // RFC 6585
                429 => 'Too Many Requests', // RFC 6585
                431 => 'Request Header Fields Too Large', // RFC 6585
                444 => 'No Response', // Nginx
                449 => 'Retry With', // Microsoft
                450 => 'Blocked by Windows Parental Controls', // Microsoft
                451 => 'Unavailable For Legal Reasons', // Internet draft
                451 => 'Redirect', // Microsoft
                494 => 'Request Header Too Large', // Nginx
                495 => 'Cert Error', // Nginx
                496 => 'No Cert', // Nginx
                497 => 'HTTP to HTTPS', // Nginx
                499 => 'Client Closed Request', // Nginx
                500 => 'Internal Server Error',
                501 => 'Not Implemented',
                502 => 'Bad Gateway',
                503 => 'Service Unavailable',
                504 => 'Gateway Timeout',
                505 => 'HTTP Version Not Supported',
                506 => 'Variant Also Negotiates', // RFC 2295
                507 => 'Insufficient Storage', // WebDAV; RFC 4918
                508 => 'Loop Detected', // WebDAV; RFC 5842
                509 => 'Bandwidth Limit Exceeded', // Apache bw/limited extension
                510 => 'Not Extended', // RFC 2774
                511 => 'Network Authentication Required', // RFC 6585
                598 => 'Network read timeout error', // Unknown
                599 => 'Network connect timeout error', // Unknown
            ];
        }
        return self::$status;
    }
}
