<?php
    
    namespace MetaNull\RESTfulService\Network\Http;

    use \Exception;
    
    /**
     * Extends exception to facilitate "throwing" http error codes.
     * When the Application receives such an exception it shall interrupt the processing, and return a HTTP response with the provided status code and HTTP headers
     *
     * @author Pascal Havelange (havelangep@hotmail.com)
     */
class HttpException extends Exception
{
    private ?Response $response = null;
    
    /**
     * @param int|Response $response A HTTP Status code, or a Response object.
     */
    public function __construct($response = 400)
    {
        if (is_int($response)) {
            $this->constructFromCode($response);
        } else {
            $this->constructFromResponse($response);
        }
    }

    /**
     * @param int $code The HTTP status code
     */
    private function constructFromCode(int $code)
    {
        $this->response = new Response($code);
    }

    /**
     * @param Response $response The HTTP Response
     */
    private function constructFromResponse(Response $response)
    {
        $this->response = $response;
    }
        
    /**
     * Get the HTTP Response that is to be returned after handling this exception
     * @return Response
     */
    public function GetResponse() : Response
    {
        return $this->response;
    }
}
