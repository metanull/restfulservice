<?php

namespace MetaNull\RESTfulService\Network\Http\Route;

use MetaNull\RESTfulService\Network\Http\RequestHandlerInterface;
use MetaNull\RESTfulService\Network\Http\Request;

/**
 * A RouteHandler is a RequestHandler that is capable of verifying if it is capable to handle a specific request.
 * @author Pascal Havelange (havelangep@hotmail.com)
 */
interface RouteHandlerInterface extends RequestHandlerInterface
{

    /**
     * A RouteHandler is a RequestHandler that is capable of verifying if it is capable to handle a specific request.
     * @param Request $request The HTTP request to handle
     * @param array & $matches May be used by the handler to return some data
     * @return bool The function shall return true if it is capable of handling the request, or false otherwise
     */
    public function Handles(Request $request, array &$matches = null) : bool;
}
