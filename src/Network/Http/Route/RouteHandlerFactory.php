<?php

namespace MetaNull\RESTfulService\Network\Http\Route;

use MetaNull\RESTfulService\Network\Http\Route\RouteHandlerInterface;
use \ReflectionClass;
use \InvalidArgumentException;

/**
 * A RouteHandler is a RequestHandler that is capable of verifying if it is capable to handle a specific request.
 * @author Pascal Havelange (havelangep@hotmail.com)
 */
class RouteHandlerFactory
{

    /**
     * @param string $routeHandlerClassName Class of RouteHandler to be created (that class shall implement RouteHandlerInterface, or extend predefined route handler classes such as AnyRouteHandler,StringMatchRouteHandler,RegexpMatchRouteHandler...)
     * @param mixed ...$arguments Parameters to pass to the class constructor
     * @return RouteHandlerInterface
     * @throws \InvalidArgumentException
     */
    public static function create(string $routeHandlerClassName, ...$arguments) : RouteHandlerInterface
    {
        $class = new ReflectionClass($routeHandlerClassName);
        if (!$class->implementsInterface(RouteHandlerInterface::class)) {
            throw new InvalidArgumentException(sprintf('Class %s does not implement interface %s.', $routeHandlerClassName, RouteHandlerInterface::class));
        }
        $routeHandler = $class->newInstance(...$arguments);
        // $routeHandler =  new $routeHandlerClassName(...$arguments);
        return $routeHandler;
    }
}
