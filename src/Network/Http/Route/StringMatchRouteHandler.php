<?php

namespace MetaNull\RESTfulService\Network\Http\Route;

use MetaNull\RESTfulService\Network\Http\Request;

/**
 * A route handler that uses (strict) string comparison to determine if it can handle the received request.
 * The route is compared with the expression using '===' (two operands must be of type strings, comparison is case sensitive)
 */
abstract class StringMatchRouteHandler extends SpecializedRouteHandler
{
    
    /**
     * Initializes the comparison expression
     * @param string $expression The comparison expression
     */
    public function __construct($expression = '')
    {
        parent::__construct($expression);
    }

    /**
     * A RouteHandler is a RequestHandler that is capable of verifying if it is capable to handle a specific request.
     * StringMatchRouteHandler::Handles return True if and only if the route in the Request is exactly equal to the configured expression string
     * @param Request $request The HTTP request to handle
     * @param array & $matches May be used by the handler to return some data
     * @return bool The function shall return true if it is capable of handling the request, or false otherwise
     */
    public function Handles(Request $request, array &$matches = null) : bool
    {
        $matches = [];
        if ($request->route === $this->GetRouteExpression()) {
            $matches[] = $request->route;
            return true;
        }
        return false;
    }
}
