<?php

namespace MetaNull\RESTfulService\Network\Http\Route;

/**
 * A route handler that uses an "expression" to determine if it is capable of processing the received request.
 * It is left to the child class to define how they will compare that expression with the received route
 * @author Pascal Havelange (havelangep@hotmail.com)
 */
abstract class SpecializedRouteHandler implements RouteHandlerInterface
{
    /** @var string $expression The comparison expression */
    private string $expression = '';

    /**
     * Initializes the comparison expression
     * @param string $expression The comparison expression
     */
    public function __construct($expression)
    {
        $this->SetRouteExpression($expression);
    }

    /**
     * Initializes the comparison expression
     * @param string $expression the expression
     */
    public function SetRouteExpression(string $expression)
    {
        $this->expression = $expression;
    }
    /**
     * Retrieves the comparison expression
     * @return string The expression
     */
    public function GetRouteExpression() : string
    {
        return $this->expression;
    }
}
