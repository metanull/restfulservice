<?php

namespace MetaNull\RESTfulService\Network\Http\Route;

use MetaNull\RESTfulService\Network\Http\Request;

/**
 * A route handler that will process ANY request it receives
 * @author Pascal Havelange (havelangep@hotmail.com)
 */
abstract class AnyRouteHandler implements RouteHandlerInterface
{

    /**
     * A RouteHandler is a RequestHandler that is capable of verifying if it is capable to handle a specific request.
     * This function will always return true (as it can handle Any route)
     * @param Request $request The HTTP request to handle
     * @param array & $matches May be used by the handler to return some data
     * @return bool The function shall return true if it is capable of handling the request, or false otherwise
     */
    public function Handles(Request $request, array &$matches = null) : bool
    {
        $matches = [];
        return true;
    }
}
