<?php

namespace MetaNull\RESTfulService\Network\Http\Route;

use MetaNull\RESTfulService\Network\Http\Request;

abstract class RegexpMatchRouteHandler extends SpecializedRouteHandler
{

    private $preg_marker = '~';
    /**
     * Initializes the comparison expression
     * @param string $expression The comparison expression
     * @param string $preg_marker Perl regular expression use a marker at the beginning and end of the expression. Example: /^hello/  '/' is the marker, ^hello is the expresion
     */
    public function __construct($expression = '^$', $preg_marker = '~')
    {
        parent::__construct($expression);
        $this->preg_marker = $preg_marker;
    }

    /**
     * A RouteHandler is a RequestHandler that is capable of verifying if it is capable to handle a specific request.
     * RegexpMatchRouteHandler::Handles return True if and only if the route in the Request matches with the configured expression using Perl Regular Expression matching.
     * @param Request $request The HTTP request to handle
     * @param array & $matches May be used by the handler to return some data
     * @return bool The function shall return true if it is capable of handling the request, or false otherwise
     */
    public function Handles(Request $request, array &$matches = null) : bool
    {
        $matches = [];
        $preg_expression = sprintf('%s%s%s',$this->preg_marker,$this->GetRouteExpression(),$this->preg_marker);
        if (1 === preg_match($preg_expression, $request->route, $matches, PREG_UNMATCHED_AS_NULL)) {
            array_shift($matches); // The first match is the full route, let's remove it
            return true;
        }
        return false;
    }
}
