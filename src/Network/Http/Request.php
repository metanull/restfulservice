<?php
    namespace MetaNull\RESTfulService\Network\Http;
    
    use \JsonException;

    /**
     * Encapsulates a HTTP Request
     * The constructor initializes the Request object with the details of the HTTP request currently being processed by PHP
     *
     * @author Pascal Havelange (havelangep@hotmail.com)
     */
class Request extends Http
{
    /** @var string HTTP verb (e.g.: GET)*/
    public ?string $method = null;
    /** @var string HTTP protocol version (e.g.: HTTP/1.1) */
    public ?string $protocol = null;
    /** @var string IP address of the client (if the client is behind a proxy, its ip address is fetched from HTTP_X_FORWARDED_FOR header) */
    public ?string $clientIp = null;
    /** @var string The request's HOST */
    public ?string $host = null;
    /** @var string The request's URI */
    public ?string $uri = null;
    /** @var string The request's Query String */
    public ?string $queryString = null;
    /** @var array An associative array of the parameters received from the query string (note: the 'route' parameters is used internally by this framework, it is removed from the array) */
    public ?array $parameters = null;
    /** @var string The value of the Content-Type header */
    public ?string $contentType = null;
    /** @var array If the request has a body, and if we can parse it automatically, parsedBody will contain teh parsed body, as an associative array */
    public ?array $parsedBody = null;
    /** @var string The value of the route paramter (received though the query string) */
    public ?string $route = null;

}
