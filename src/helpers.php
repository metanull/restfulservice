<?php

namespace MetaNull\RESTfulService;

use MetaNull\RESTfulService\Network\Http\HttpException;
use MetaNull\RESTfulService\Network\Http\ResponseFactory;

use \DomDocument;
use \DOMXPath;

class Helpers
{
    /**
     * HTML Syntax highlighting for a php variable
     * @param mixed $variable
     * @return string
     */
    public static function highlightVariable($variable) : string
    {
        return highlight_string(sprintf('<?= %s ?>', var_export($variable, true)), true);
    }

    /**
     * "Throws" an HTTP Exception setting the response body to the 'HTML syntax highlighted' value of the passed variable
     * @param mixed $variable
     * @throws HTTPException
     */
    public static function dumpVariable($variable)
    {
        throw new HttpException(ResponseFactory::Html(self::highlightVariable($variable)));
    }

    /**
     * A better alternative to strip_tags, using DOM's HTML parser and XPath.
     * @param string $html
     * @return string
     * @see strip_tags
     */
    public static function HtmlToText(string $html) : string
    {
        $doc = new DOMDocument();
        $doc->loadHTML($html);
        $xpath = new DOMXPath($doc);
        $text = '';
        foreach ($xpath->query('//text()') as $node) {
            $t = $node->textContent;
            if (!empty($t)) {
                $text .= $t;
            }
        }
        return $text;
    }
}
