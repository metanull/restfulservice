<?php

namespace MetaNull\RESTfulService\Application;

interface ApplicationInterface
{
    
    public function Run();
}
