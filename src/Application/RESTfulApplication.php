<?php
    namespace MetaNull\RESTfulService\Application;
    
    use \Exception;
    use \RuntimeException;
    use \LogicException;

    use MetaNull\RESTfulService\Application\WebApplicationInterface;
    
    use MetaNull\RESTfulService\Network\Ip\IpException;
    use MetaNull\RESTfulService\Network\Ip\IpV4;

    use MetaNull\RESTfulService\Network\Http\Http;
    use MetaNull\RESTfulService\Network\Http\HttpException;

    use MetaNull\RESTfulService\Network\Http\Method\Delete;
    use MetaNull\RESTfulService\Network\Http\Method\Get;
    use MetaNull\RESTfulService\Network\Http\Method\Head;
    use MetaNull\RESTfulService\Network\Http\Method\Options;
    use MetaNull\RESTfulService\Network\Http\Method\Patch;
    use MetaNull\RESTfulService\Network\Http\Method\Post;
    use MetaNull\RESTfulService\Network\Http\Method\Put;

    use MetaNull\RESTfulService\Network\Http\Request;
    use MetaNull\RESTfulService\Network\Http\RequestFactory;
    use MetaNull\RESTfulService\Network\Http\Response;
    use MetaNull\RESTfulService\Network\Http\ResponseFactory;

    use MetaNull\RESTfulService\Network\Http\Route\RouteHandlerFactory;

    /**
     * RESTfulApplication encapsulates the basic functionalities of a RESTful Web service
     * Calling its Serve() method processes the received request, calls Routes to generate a response, and then sends that response to the client
     * Exceptions are captured and handeled. Critical errors would result in a HTTP 500 response, and logs; other errors with result in the corresponding HTTP error response (e.g.: HTTP 404)
     *
     * @author Pascal Havelange (havelangep@hotmail.com)
     */
class RESTfulApplication implements WebApplicationInterface
{
    /** @var array The list of request handlers. Structured as follows:  [ {VERB} => [ {INSTANCE OF REQUEST_HANDLER} ]] */
    protected array $routes = [];
    /** @var array The list of middlewares. Structured as follows:  [ {VERB} => [ {INSTANCE OF REQUEST_HANDLER} ]] */
    protected array $middlewares = [];
    /** @var Request The received HTTP Request */
    protected ?Request $request = null;
    /** @var Response The response that will be sent to the client */
    protected ?Response $response = null;

    /** @var array List of supported HTTP Methods */
    const METHODS = [Http::OPTIONS,Http::HEAD,Http::GET,Http::POST,Http::PUT,Http::DELETE,Http::PATCH];
    /** @var array List of supported HTTP Protocol Versions */
    const PROTOCOLS = [Http::HTTP1_1, Http::HTTP2];
        
    /** Initialize the web service */
    public function __construct()
    {
        $this->request = RequestFactory::FromCurrentRequest();
        $this->response = ResponseFactory::NotFound();

        $this->routes = [
            Http::DELETE=>[],
            Http::GET=>[],
            Http::HEAD=>[],
            Http::OPTIONS=>[],
            Http::PATCH=>[],
            Http::POST=>[],
            Http::PUT=>[],
        ];

        $this->middlewares = [
            Http::DELETE=>[],
            Http::GET=>[],
            Http::HEAD=>[],
            Http::OPTIONS=>[],
            Http::PATCH=>[],
            Http::POST=>[],
            Http::PUT=>[],
        ];
    }

    public function GetRequest() : Request {
        $request = clone $this->request;
        return $request;
    }
    public function & GetResponse() : Response {
        return $this->response;
    }
        
    /** Initialize the web service: Store data received from the client in the Service object; perform controls on the received data */
    protected function Initialize()
    {
        if (null === $this->request->clientIp) {
            throw new RuntimeException('Client IP not available');
        }
        if (null === $this->request->route) {
            throw new HttpException(400);
        }
        if (!in_array($this->request->protocol, self::PROTOCOLS)) {
            throw new HttpException(400);
        }
        if (!in_array($this->request->method, self::METHODS)) {
            throw new HttpException(400);
        }
        
        /**
         * Remove when development of the framework is stable enough
         */
        $this->response->SetHeader('X-CLIENT-IP', $this->request->clientIp);
        $this->response->SetHeader('X-REQUEST-PROTOCOL', $this->request->protocol);
        $this->response->SetHeader('X-REQUEST-METHOD', $this->request->method);
        $this->response->SetHeader('X-ROUTE', $this->request->route);
        $this->response->SetHeader('X-QUERY-STRING', $this->request->queryString);
        $this->response->SetHeader('X-REQUEST-HEADERS', json_encode($this->request->headers));
        $this->response->SetHeader('X-REQUEST-PARAMETERS', json_encode($this->request->parameters));
        $this->response->SetHeader('X-REQUEST-BODY', json_encode($this->request->body));
        $this->response->SetHeader('X-REQUEST-PARSEDBODY', json_encode($this->request->parsedBody));
    }
        
    public function & Route($classname, ...$arguments) : WebApplicationInterface
    {
        $handler = RouteHandlerFactory::create($classname, ...$arguments);
        if ($handler instanceof Delete) {
            $this->routes[Http::DELETE][] = $handler;
        }
        if ($handler instanceof Get) {
            $this->routes[Http::GET][] = $handler;
        }
        if ($handler instanceof Head) {
            $this->routes[Http::HEAD][] = $handler;
        }
        if ($handler instanceof Options) {
            $this->routes[Http::OPTIONS][] = $handler;
        }
        if ($handler instanceof Patch) {
            $this->routes[Http::PATCH][] = $handler;
        }
        if ($handler instanceof Post) {
            $this->routes[Http::POST][] = $handler;
        }
        if ($handler instanceof Put) {
            $this->routes[Http::PUT][] = $handler;
        }
        return $this;
    }

    public function & Middleware($classname, ...$arguments) : WebApplicationInterface
    {
        $handler = RouteHandlerFactory::create($classname, ...$arguments);
        if ($handler instanceof Delete) {
            $this->middlewares[Http::DELETE][] = $handler;
        }
        if ($handler instanceof Get) {
            $this->middlewares[Http::GET][] = $handler;
        }
        if ($handler instanceof Head) {
            $this->middlewares[Http::HEAD][] = $handler;
        }
        if ($handler instanceof Options) {
            $this->middlewares[Http::OPTIONS][] = $handler;
        }
        if ($handler instanceof Patch) {
            $this->middlewares[Http::PATCH][] = $handler;
        }
        if ($handler instanceof Post) {
            $this->middlewares[Http::POST][] = $handler;
        }
        if ($handler instanceof Put) {
            $this->middlewares[Http::PUT][] = $handler;
        }
        return $this;
    }

    /**
     * "Run" the service: parse the received request and call the appropriate handlers to generate a response
     */
    final public function Run()
    {
        try {
            // Initialization
            if (false === ob_start()) {
                throw new RuntimeException('Output buffering not available');
            }
            $this->Initialize();
            
            $matches = [];
            $handled = false;
            
            // Sanity check (handle case where request has a VERB for which no route was registered)
            if (!array_key_exists($this->request->method, $this->routes)) {
                throw new HttpException(ResponseFactory::NotFound());
            }

            // Execute middlewares
            foreach (($this->middlewares[$this->request->method] ?? []) as $handler) {
                if ($handler->Handles($this->request, $matches)) {
                    trigger_error(sprintf('Running middleware %s on %s %s', get_class($handler), $this->request->method, $this->request->route), E_USER_NOTICE);
                    $handler->Handle($this->request, $this->response, ...$matches);
                }
            }

            // Handle the request
            foreach (($this->routes[$this->request->method] ?? []) as $handler) {
                if ($handler->Handles($this->request, $matches)) {
                    trigger_error(sprintf('Running route %s on %s %s', get_class($handler), $this->request->method, $this->request->route), E_USER_NOTICE);
                    $handled = true;
                    $handler->Handle($this->request, $this->response, ...$matches);
                    break;
                }
            }
                
            // If we couldn't find any suitable handler for the request, check if it could have been through other VERBs => 405, otherwise =>404
            if (false === $handled) {
                $supported = [];
                foreach ($this->routes as $method => $handlers) {
                    foreach ($handlers as $handler) {
                        if ($handler->Handles($this->request, $matches)) {
                            trigger_error(sprintf('Found handler %s on %s %s, but %s was requested', get_class($handler), $this->request->method, $this->request->route, $method), E_USER_NOTICE);
                            $supported[] = $method;
                            break;
                        }
                    }
                }
                if (count($supported)) {
                    $this->response = ResponseFactory::MethodNotAllowed($supported);
                } else {
                    // Here we could add code to handle the HTTP 404 via a user defined function
                    // In fact we could register a set of functions for various HTTP status codes
                    trigger_error(sprintf('Couldn\'t find a suitable route for %s %s. No other HTTP verbs are supported.', $this->request->method, $this->request->route), E_USER_WARNING);
                    throw new HttpException(ResponseFactory::NotFound());
                }
            }
        } catch (HttpException $e) {
            // Change the HTTP response with the one received from the exception
            $this->response = $e->GetResponse();
            // Logging the error with E_USER_NOTICE, to avoid breaking execution, and also because HTTP Exception is used by this framework to signal 'normal' errors
            trigger_error(sprintf('HTTP Exception in %s %s: %s', $this->request->method, $this->request->route, $this->response->firstLine), E_USER_NOTICE);
        } catch (Exception $e) {
            // Logging the error with E_USER_WARNING, to avoid breaking execution
            trigger_error(sprintf('Exception in %s %s%s%s: %d %s', $this->request->method, $this->request->route, PHP_EOL, get_class($e), $e->getCode(), $e->getMessage()), E_USER_WARNING);
            // Then return a HTTP 500, with no details
            $this->response = ResponseFactory::InternalServerError();
        } finally {
            # Check if data was accidentally sent to stdout, and log it
            if (ob_get_length()>0) {
                $content = ob_get_contents();
                trigger_error(sprintf('<<<STDOUT>%s%s%s<<</STDOUT>>>%s', PHP_EOL, $content, PHP_EOL, PHP_EOL), E_USER_NOTICE);
            }
            ob_end_clean();
            # Send the response back
            $this->response->Send();
        }
    }
}
