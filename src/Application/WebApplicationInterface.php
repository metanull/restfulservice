<?php

namespace MetaNull\RESTfulService\Application;

use MetaNull\RESTfulService\Network\Http\Request;
use MetaNull\RESTfulService\Network\Http\Response;

interface WebApplicationInterface extends ApplicationInterface
{
    public function & Route($classname, ...$arguments) : WebApplicationInterface;
    public function & Middleware($classname, ...$arguments) : WebApplicationInterface;

    public function GetRequest() : Request;
    public function & GetResponse() : Response;
}
