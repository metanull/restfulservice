<?php

function autoload($class)
{
    // Define an array of namespace prefixes and their corresponding base directory paths
    $prefixes = [
        'MetaNull\\' => __DIR__ . '/src/MetaNull',
    ];

    // Iterate over the prefixes and try to match the class with the corresponding file path
    foreach ($prefixes as $prefix => $baseDir) {
        // Check if the class uses the current namespace prefix
        $len = strlen($prefix);
        if (strncmp($prefix, $class, $len) !== 0) {
            continue;
        }
        
        // Remove the namespace prefix and convert the namespace separators to directory separators
        $relativeClass = substr($class, $len);
        $relativeClass = str_replace('\\', '/', $relativeClass);
        
        // Construct the file path by appending the base directory path and the relative class name with '.php' extension
        $file = $baseDir . '/' . $relativeClass . '.php';
        
        // Require the file if it exists
        if (file_exists($file)) {
            require_once $file;
        }
    }
}

// Register the autoloader function with spl_autoload_register()
spl_autoload_register('autoload');
